#!/bin/bash

queueID=$1
WorkDir='/afs/cern.ch/user/r/rwakasa/SCTAxAOD/SCTAxAOD/standalone/script/TightEfficiency/'

echo $WorkDir

submitList=(`cat $WorkDir'submitList.txt'`)
echo ${submitList[$queueID]}

outputPath='/eos/atlas/unpledged/group-tokyo/users/rwakasa/SCT/condorOut/'
outName='TightEfficiency_200204_v2'
inputFileDir='/eos/atlas/unpledged/group-tokyo/users/rwakasa/SCT/355331_physics_Main_TightPrimary/user.rwakasa.user.rwakasa.00355331.physics.Main.small.RAW_v2_tightPrimary_200202_EXT1.SCTAxAOD_200203_histograms.root/'

if test ! -d $outputPath$outName ; then
    mkdir $outputPath$outName
fi

shift
shift
shift
shift
shift
shift
shift
shift
shift

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
echo ${ATLAS_LOCAL_ROOT_BASE}
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
#cd /afs/cern.ch/user/r/rwakasa/SCTAxAOD/SCTAxAOD/
#lsetup rcsetup
#lsetup "root 6.14.04-x86_64-slc6-gcc62-opt"
lsetup "root 6.18.04-x86_64-centos7-gcc8-opt"

sample=$inputFileDir${submitList[$queueID]}

echo $sample

if test ! -d $outputPath$outName'/'${submitList[$queueID]} ; then
    mkdir $outputPath$outName'/'${submitList[$queueID]}
fi


cd $WorkDir
./EfficiencyLoopInterStrip $sample $outputPath$outName'/'${submitList[$queueID]} > $outputPath$outName'/'${submitList[$queueID]}'.log'

# /eos/user/r/rwakasa/SCTAxAOD/user.rwakasa.data18_13TeV.00355331.physics_Main.daq.RAW._lb200_lb300.190517_EXT1.output_1_histograms.root/ /afs/cern.ch/work/r/rwakasa/hotono/SCT/healthyModules.txt
#cd /afs/cern.ch/user/r/rwakasa/SCTAxAOD/SCTAxAOD/standalone/bin/
#./EfficiencyLoopXY /eos/user/r/rwakasa/SCTAxAOD/user.rwakasa.data18_13TeV.00355331.physics_Main.daq.RAW._lb200_lb300.190517_EXT1.output_1_histograms.root/ /afs/cern.ch/work/r/rwakasa/hotono/SCT/healthyModules.txt
