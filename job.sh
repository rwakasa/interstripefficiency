executable = /afs/cern.ch/user/r/rwakasa/SCTAxAOD/SCTAxAOD/standalone/script/submitHTCondor.sh
#executable = /afs/cern.ch/user/r/rwakasa/SCTAxAOD/SCTAxAOD/standalone/script/submitHTCondor_src_other_tight.sh
arguments  = $(ProcId)
output = /afs/cern.ch/user/r/rwakasa/SCTAxAOD/SCTAxAOD/standalone/script/condor/out.log
error = /afs/cern.ch/user/r/rwakasa/SCTAxAOD/SCTAxAOD/standalone/script/condor/err.log
log = /afs/cern.ch/user/r/rwakasa/SCTAxAOD/SCTAxAOD/standalone/script/condor/job.log
+MaxRunTime = 10000

queue 420
