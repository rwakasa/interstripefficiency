#include <iostream>
#include <fstream>
#include <math.h>

#include "TROOT.h"
#include "TApplication.h"
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TH2.h"
#include "THStack.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TCanvas.h"
#include "TString.h"
#include "TLeaf.h"
#include "TStyle.h"

enum pt {
  pt1GeV,
  pt5GeV,
  pt10GeV
};
const char *nm_pt[] = {
  [pt1GeV]  = "pt1GeV",
  [pt5GeV]  = "pt5GeV",
  [pt10GeV] = "pt10GeV"
};
enum phi {
  phim20,
  phim15,
  phim10,
  phim5,
  phi0,
  phi_other
};
const char *nm_phi[] = {
  [phim20] = "phim20",
  [phim15] = "phim15",
  [phim10] = "phim10",
  [phim5]  = "phim5",
  [phi0]   = "phi0",
};
enum::phi SetPhi(float incphi){
  if(incphi>-22.5 && incphi<-17.5){
    return phim20;
  }else if(incphi>=-17.5 && incphi<-12.5){
    return phim15;
  }else if(incphi>=-12.5 && incphi<-7.5){
    return phim10;
  }else if(incphi>=-7.5 && incphi<-2.5){
    return phim5;
  }else if(incphi>=-2.5 && incphi<2.5){
    return phi0;
  }else{
    return phi_other;
  }
}

enum theta {
  thetam40,
  thetam20,
  theta0,
  theta20,
  theta40,
  theta_other
};
const char *nm_theta[] = {
  [thetam40] = "thetam40",
  [thetam20] = "thetam20",
  [theta0]   = "theta0",
  [theta20]  = "theta20",
  [theta40]  = "theta40",
};
enum::theta SetTheta(float inctheta){
  if(inctheta>-50. && inctheta<=-30.){
    return thetam40;
  }else if(inctheta>-30. && inctheta<=-10.){
    return thetam20;
  }else if(inctheta>-10. && inctheta<=10.){
    return theta0;
  }else if(inctheta>10. && inctheta<=30.){
    return theta20;
  }else if(inctheta>30. && inctheta<=50.){
    return theta40;
  }else{
    return theta_other;
  }
}


int main(int argc, char *argv[]){
  
  TString fileName = std::string(argv[1]);
  TString outputFile = std::string(argv[2])+"/TightEfficinecy.root";
  TString text = "/afs/cern.ch/work/r/rwakasa/hotono/SCT/healthyModules.txt";
  //  TFile *f = new TFile(fileName);
  std::cout << fileName << std::endl;

  TChain *ch = new TChain("track");
  ch->Add(fileName);

  // Get information of healthy modules
  std::ifstream fin(text);
  int bt = 0;
  char buf[256];
  while(fin.getline(buf,sizeof(buf))){
    bt++;
  }
  fin.close();
  if(bt==0){
    std::cerr << "Error:" << text << " does not exist!" << std::endl;
    return -1;
  }

  int bec_index[bt], layer_index[bt], eta_index[bt], phi_index[bt];
  std::ifstream fi(text);
  for(int i=0; i<bt; i++){
    fi >> bec_index[i] >> layer_index[i] >> eta_index[i] >> phi_index[i];
  }
  fi.close();
  /** Here user parameters **/
  /**************************/
  std::vector<int> *getHitBec   = 0;
  std::vector<int> *getHitLayer = 0;
  std::vector<int> *getHitEta   = 0;
  std::vector<int> *getHitPhi   = 0;
  std::vector<int> *getHitSide  = 0;
  std::vector<float> *getHitLocalX = 0;
  std::vector<float> *getHitLocalY = 0; 
  std::vector<float> *getHitLocalphi = 0;
  std::vector<float> *getHitLocaltheta = 0;
  std::vector<int> *getHoleBec   = 0;
  std::vector<int> *getHoleLayer = 0;
  std::vector<int> *getHoleEta   = 0;
  std::vector<int> *getHolePhi   = 0;
  std::vector<int> *getHoleSide  = 0;
  std::vector<float> *getHoleLocalX = 0;
  std::vector<float> *getHoleLocalY = 0;
  std::vector<float> *getHoleLocalphi = 0;
  std::vector<float> *getHoleLocaltheta = 0;
  std::vector<int> *getHitStrip = 0;
  std::vector<int> *getHitClusize = 0;
  Int_t getLb = 0;
  Int_t getTrack = 0;
  Int_t getBCID = 0;
  Float_t getPt = 0;
  Float_t getPti = 0;
  Float_t getD0 = 0;
  Float_t getZ0 = 0;
  Float_t getChiSquared = 0;
  Float_t getDoF = 0;
  Float_t getTrketa = 0.;
  Int_t getSCThits = 0;
  Int_t getSCTouts = 0;
  Int_t getPixelhits = 0;
  Int_t getPixelouts = 0;
  Int_t getSCTdholes = 0;
  Int_t getSCTsharehits = 0;
  Int_t getSCTdsensors = 0;
  Int_t getSCTspoilthits = 0;
  Int_t getSCTholes = 0;
  Int_t getPixelholes = 0;
  Int_t getIBLehits = 0;
  Int_t getIBLhits = 0;
  Int_t getIBLouts = 0;
  Int_t getBlayerehits = 0;
  Int_t getBlayerhits = 0;
  Int_t getBlayerouts = 0;
  
  int nBranches = ch->GetNbranches();

  ch->SetBranchAddress("hit_bec",     &getHitBec);
  ch->SetBranchAddress("hit_layer",   &getHitLayer);
  ch->SetBranchAddress("hit_eta",     &getHitEta);
  ch->SetBranchAddress("hit_phi",     &getHitPhi);
  ch->SetBranchAddress("hit_side",    &getHitSide);
  ch->SetBranchAddress("hit_localx", &getHitLocalX);
  ch->SetBranchAddress("hit_localy", &getHitLocalY);
  ch->SetBranchAddress("hit_localphi", &getHitLocalphi);
  ch->SetBranchAddress("hit_localtheta", &getHitLocaltheta);
  ch->SetBranchAddress("hole_bec",     &getHoleBec);
  ch->SetBranchAddress("hole_layer",   &getHoleLayer);
  ch->SetBranchAddress("hole_eta",     &getHoleEta);
  ch->SetBranchAddress("hole_phi",     &getHolePhi);
  ch->SetBranchAddress("hole_side",    &getHoleSide);
  ch->SetBranchAddress("hole_localx", &getHoleLocalX);
  ch->SetBranchAddress("hole_localy", &getHoleLocalY);
  ch->SetBranchAddress("hole_localphi", &getHoleLocalphi);
  ch->SetBranchAddress("hole_localtheta", &getHoleLocaltheta);
  ch->SetBranchAddress("hit_strip", &getHitStrip);
  ch->SetBranchAddress("hit_clusize", &getHitClusize);
  ch->SetBranchAddress("lb", &getLb); 
  ch->SetBranchAddress("track", &getTrack);
  ch->SetBranchAddress("bcid", &getBCID);
  ch->SetBranchAddress("pt", &getPt);
  ch->SetBranchAddress("pti", &getPti);
  ch->SetBranchAddress("d0", &getD0);
  ch->SetBranchAddress("z0", &getZ0);
  ch->SetBranchAddress("chiSquared", &getChiSquared);
  ch->SetBranchAddress("dof", &getDoF);
  ch->SetBranchAddress("trketa", &getTrketa);
  ch->SetBranchAddress("nhit_SCT", &getSCThits);
  ch->SetBranchAddress("nout_SCT", &getSCTouts);
  ch->SetBranchAddress("nhit_Pixel", &getPixelhits);
  ch->SetBranchAddress("nout_Pixel", &getPixelouts);
  ch->SetBranchAddress("ndhole_SCT", &getSCTdholes);
  ch->SetBranchAddress("nsharehit_SCT", &getSCTsharehits);
  ch->SetBranchAddress("ndsensor_SCT", &getSCTdsensors);
  ch->SetBranchAddress("nspoilthit_SCT", &getSCTspoilthits);
  ch->SetBranchAddress("nhole", &getSCTholes);
  ch->SetBranchAddress("nhole_Pixel", &getPixelholes);
  ch->SetBranchAddress("nehit_InnermostPixel", &getIBLehits);
  ch->SetBranchAddress("nhit_InnermostPixel", &getIBLhits);
  ch->SetBranchAddress("nout_InnermostPixel", &getIBLouts);
  ch->SetBranchAddress("nehit_NextInnermostPixel", &getBlayerehits);
  ch->SetBranchAddress("nhit_NextInnermostPixel", &getBlayerhits);
  ch->SetBranchAddress("nout_NextInnermostPixel", &getBlayerouts);

  int nEvents = ch->GetEntries();
  if(nEvents==0){
    std::cout << "Error::cannot get events! " << std::endl;
    return -1;
  }
  std::cout << "All event number = " << nEvents << std::endl;

  // set histogram
  TProfile *Strip0 = new TProfile("Strip_side0","Strip_side0",100,0.,80.);
  TProfile *Strip1 = new TProfile("Strip_side1","Strip_side1",100,0.,80.);

  TProfile *Strip_step_side0[120];
  for(int i=0; i<5; i++){
    for(int j=0; j<4; j++){
      for(int k=0; k<6; k++){
	std::string nm_hist0 = "Strip_pt1GeV_"+std::string(nm_phi[i])+"_layer"+std::to_string(j)+"_eta"+std::to_string(k+1)+"_side0";
	Strip_step_side0[i+5*j+20*k] = new TProfile(nm_hist0.c_str(),nm_hist0.c_str(),10,0.,80.);
      }
    }
  }
  TProfile *Strip_step_side1[120];
  for(int i=0; i<5; i++){
    for(int j=0; j<4; j++){
      for(int k=0; k<6; k++){
	std::string nm_hist0 = "Strip_pt1GeV_"+std::string(nm_phi[i])+"_layer"+std::to_string(j)+"_eta"+std::to_string(k+1)+"_side1";
	Strip_step_side1[i+5*j+20*k] = new TProfile(nm_hist0.c_str(),nm_hist0.c_str(),10,0.,80.);
      }
    }
  }
  TProfile *Strip_side0[5];
  for(int i=0; i<5; i++){
    std::string nm_hist0 = "Strip_pt1GeV_"+std::string(nm_phi[i])+"_side0";
    Strip_side0[i] = new TProfile(nm_hist0.c_str(),nm_hist0.c_str(),10,0.,80.);
  }
  TProfile *Strip_side1[5];
  for(int i=0; i<5; i++){
    std::string nm_hist0 = "Strip_pt1GeV_"+std::string(nm_phi[i])+"_side1";
    Strip_side1[i] = new TProfile(nm_hist0.c_str(),nm_hist0.c_str(),10,0.,80.);
  }
  TProfile *Strip_fine_side0[75];
  for(int i=0; i<3; i++){
    for(int j=0; j<5; j++){
      for(int k=0; k<5; k++){
	std::string nm_hist0 = "Strip_"+std::string(nm_pt[i])+"_"+std::string(nm_phi[j])+"_"+std::string(nm_theta[k])+"_side0";
	Strip_fine_side0[i+3*j+15*k] = new TProfile(nm_hist0.c_str(),nm_hist0.c_str(),10,0.,80.);
      }
    }
  }
  TProfile *Strip_fine_side1[75];
  for(int i=0; i<3; i++){
    for(int j=0; j<5; j++){
      for(int k=0; k<5; k++){
	std::string nm_hist0 = "Strip_"+std::string(nm_pt[i])+"_"+std::string(nm_phi[j])+"_"+std::string(nm_theta[k])+"_side1";
	Strip_fine_side1[i+3*j+15*k] = new TProfile(nm_hist0.c_str(),nm_hist0.c_str(),10,0.,80.);
      }
    }
  }
  TProfile *Strip_region_side0[20];
  for(int i=0; i<5; i++){
    for(int j=0; j<4; j++){
      std::string nm_hist0 = "Strip_pt1GeV_"+std::string(nm_phi[i])+"_region_"+std::to_string(j)+"_side0";
      Strip_region_side0[i+5*j] = new TProfile(nm_hist0.c_str(),nm_hist0.c_str(),10,0.,80.);
    }
  }
  TProfile *Strip_region_side1[20];
  for(int i=0; i<5; i++){
    for(int j=0; j<4; j++){
      std::string nm_hist0 = "Strip_pt1GeV_"+std::string(nm_phi[i])+"_region_"+std::to_string(j)+"_side1";
      Strip_region_side1[i+5*j] = new TProfile(nm_hist0.c_str(),nm_hist0.c_str(),10,0.,80.);
    }
  }
  TProfile *Strip_regionY_side0[480];
  for(int i=0; i<5; i++){
    for(int j=0; j<4; j++){
      for(int k=0; k<6; k++){
	for(int l=0; l<2; l++){
	  std::string nm_hist0 = "ResionY/Strip_pt1GeV_"+std::string(nm_phi[i])+"_layer"+std::to_string(j)+"_eta"+std::to_string(k+1)+"_regionY"+std::to_string(l)+"_side0";
	  Strip_regionY_side0[i+5*j+20*k+120*l] = new TProfile(nm_hist0.c_str(),nm_hist0.c_str(),10,0.,80.);
	}
      }
    }
  }
  TProfile *Strip_regionY_side1[480];
  for(int i=0; i<5; i++){
    for(int j=0; j<4; j++){
      for(int k=0; k<6; k++){
	for(int l=0; l<2; l++){
	  std::string nm_hist0 = "ResionY/Strip_pt1GeV_"+std::string(nm_phi[i])+"_layer"+std::to_string(j)+"_eta"+std::to_string(k+1)+"_regionY"+std::to_string(l)+"_side1";
	  Strip_regionY_side1[i+5*j+20*k+120*l] = new TProfile(nm_hist0.c_str(),nm_hist0.c_str(),10,0.,80.);
	}
      }
    }
  }
  TProfile *Strip_Barrel3_side0[120];
  for(int i=0; i<5; i++){
    for(int j=0; j<4; j++){
      for(int k=0; k<6; k++){
	std::string nm_hist0 = "Barrel3_RegionPhi/Strip_pt1GeV_"+std::string(nm_phi[i])+"_eta"+std::to_string(k+1)+"_PhiIndexRegion"+std::to_string(j)+"_side0";
	Strip_Barrel3_side0[i+5*j+20*k] = new TProfile(nm_hist0.c_str(),nm_hist0.c_str(),10,0.,80.);
      }
    }
  }
  TProfile *Strip_Barrel3_side1[120];
  for(int i=0; i<5; i++){
    for(int j=0; j<4; j++){
      for(int k=0; k<6; k++){
	std::string nm_hist0 = "Barrel3_RegionPhi/Strip_pt1GeV_"+std::string(nm_phi[i])+"_eta"+std::to_string(k+1)+"_PhiIndexRegion"+std::to_string(j)+"_side1";
	Strip_Barrel3_side1[i+5*j+20*k] = new TProfile(nm_hist0.c_str(),nm_hist0.c_str(),10,0.,80.);
      }
    }
  }
  TProfile *Strip_clusize_side0[20];
  for(int i=0; i<5; i++){
    for(int j=0; j<4; j++){
      std::string nm_hist0 = "Strip_pt1GeV_"+std::string(nm_phi[i])+"_clusize"+std::to_string(j+1)+"_side0";
      Strip_clusize_side0[i+5*j] = new TProfile(nm_hist0.c_str(),nm_hist0.c_str(),10,0.,80.);
    }
  }
  TProfile *Strip_clusize_side1[20];
  for(int i=0; i<5; i++){
    for(int j=0; j<4; j++){
      std::string nm_hist0 = "Strip_pt1GeV_"+std::string(nm_phi[i])+"_clusize"+std::to_string(j+1)+"_side1";
      Strip_clusize_side1[i+5*j] = new TProfile(nm_hist0.c_str(),nm_hist0.c_str(),10,0.,80.);
    }
  }
  TProfile *Strip_chi_side0[20];
  for(int i=0; i<5; i++){
    for(int j=0; j<4; j++){
      std::string nm_hist0 = "Strip_pt1GeV_"+std::string(nm_phi[i])+"_chi"+std::to_string(j)+"_side0";
      Strip_chi_side0[i+5*j] = new TProfile(nm_hist0.c_str(),nm_hist0.c_str(),10,0.,80.);
    }
  }
  TProfile *Strip_chi_side1[20];
  for(int i=0; i<5; i++){
    for(int j=0; j<4; j++){
      std::string nm_hist0 = "Strip_pt1GeV_"+std::string(nm_phi[i])+"_chi"+std::to_string(j)+"_side1";
      Strip_chi_side1[i+5*j] = new TProfile(nm_hist0.c_str(),nm_hist0.c_str(),10,0.,80.);
    }
  }

  TProfile *Clusize_side0[120];
  for(int i=0; i<5; i++){
    for(int j=0; j<4; j++){
      for(int k=0; k<6; k++){
	std::string nm_hist0 = "Clusize_pt1GeV_"+std::string(nm_phi[i])+"_layer"+std::to_string(j)+"_eta"+std::to_string(k+1)+"_side0";
	Clusize_side0[i+5*j+20*k] = new TProfile(nm_hist0.c_str(),nm_hist0.c_str(),10,0.,80.);
      }
    }
  }
  TProfile *Clusize_side1[120];
  for(int i=0; i<5; i++){
    for(int j=0; j<4; j++){
      for(int k=0; k<6; k++){
	std::string nm_hist0 = "Clusize_pt1GeV_"+std::string(nm_phi[i])+"_layer"+std::to_string(j)+"_eta"+std::to_string(k+1)+"_side1";
	Clusize_side1[i+5*j+20*k] = new TProfile(nm_hist0.c_str(),nm_hist0.c_str(),10,0.,80.);
      }
    }
  }
  TProfile *Ntrk_side0[120];
  for(int i=0; i<5; i++){
    for(int j=0; j<4; j++){
      for(int k=0; k<6; k++){
	std::string nm_hist0 = "Ntrk_pt1GeV_"+std::string(nm_phi[i])+"_layer"+std::to_string(j)+"_eta"+std::to_string(k+1)+"_side0";
	Ntrk_side0[i+5*j+20*k] = new TProfile(nm_hist0.c_str(),nm_hist0.c_str(),10,0.,80.);
      }
    }
  }
  TProfile *Ntrk_side1[120];
  for(int i=0; i<5; i++){
    for(int j=0; j<4; j++){
      for(int k=0; k<6; k++){
	std::string nm_hist0 = "Ntrk_pt1GeV_"+std::string(nm_phi[i])+"_layer"+std::to_string(j)+"_eta"+std::to_string(k+1)+"_side1";
	Ntrk_side1[i+5*j+20*k] = new TProfile(nm_hist0.c_str(),nm_hist0.c_str(),10,0.,80.);
      }
    }
  }


  int tightEvents = 0;
  for( int i=0; i<nEvents; i++ ){
    ch->GetEntry(i);
    int nHitElements = getHitBec->size();
    int nHoleElements = getHoleBec->size();
    
    float chiSquared = 0;
    chiSquared = getChiSquared/getDoF;
    float nscthits = getSCThits + getSCTouts;
    float npixelhits = getPixelhits + getPixelouts;
    int nSihits = (int)nscthits + (int)npixelhits;
    int nSiholes = getSCTholes + getPixelholes;
    
    bool f_nSi = false;
    if( fabs(getTrketa)<=1.65 ){
      if( nSihits>=9 ) f_nSi = true;
    }else{
      if( nSihits>=11 ) f_nSi = true;
    }

    bool f_nIBLBlayer = false;
    if( (getIBLhits + getBlayerhits) > 0 ) f_nIBLBlayer = true;

    ////////////////////////////
    ////////  Hit loop  ////////
    ////////////////////////////
    for( int j=0; j<nHitElements; j++ ){
      
      // only Barrel
      if(getHitBec->at(j)!=0) continue;

      // set variables
      bool f_pt1GeV = false;
      bool f_pt5GeV = false;
      bool f_pt10GeV = false;
      float incphi = 0.;
      float inctheta = 0.;
      int localx = 0;
      float localy = 0.;
      int phiIndex = 0;
      int enum_phi = 0;
      int enum_theta = 0;

      //calculate variables
      if(getHitLocaltheta->at(j)>M_PI/2){
	inctheta = ( getHitLocaltheta->at(j) - M_PI )*180./M_PI;
      } else {
	inctheta = getHitLocaltheta->at(j)*180./M_PI;
      }
      incphi = getHitLocalphi->at(j)*180./M_PI;

      //Get healthy modules
      for(int h=0; h<bt; h++){
	if( getHitBec->at(j)!=bec_index[h] || getHitLayer->at(j)!=layer_index[h] || getHitEta->at(j)!=eta_index[h] || getHitPhi->at(j)!=phi_index[h] ) continue;

	//set variables
	localx = (int)(getHitLocalX->at(j)*1000+30680);
	if(localx>61360 || localx<0 ) continue;  // reduce 40um from sensor edge
	localy = getHitLocalY->at(j);
	phiIndex = getHitPhi->at(j);

	if(getPt>1.){
	  f_pt1GeV = true;
	  if(getPt>5.){
	    f_pt5GeV = true;
	    if(getPt>10.){
	      f_pt10GeV = true;
	    }
	  }
	}
	enum_phi = SetPhi(incphi);
	enum_theta = SetTheta(inctheta);
	//set region
	int region=-99;
	if(localx>=0 && localx<15340)       region = 0;
	if(localx>=15340 && localx<30680)   region = 1;
	if(localx>=30680 && localx<46020)   region = 2;
	if(localx>=46020 && localx<=61360)  region = 3;
	//set region for localY
	int regionY=-99;
	if(localy<-30)                      regionY = 0;
	if(localy>=-30 && localy<0)         regionY = 1;
	if(localy>=0   && localy<30)        regionY = 2;
	if(localy>=30)                      regionY = 3;
	//set region for phi_index
	int regionPhiIndex=-99;
	//	if(getHitLayer->at(j)==0) std::cout << phiIndex << std::endl;
	if(phiIndex>=28 || phiIndex<4)      regionPhiIndex = 0;
	if(phiIndex>=4  && phiIndex<12)     regionPhiIndex = 1;
	if(phiIndex>=12 && phiIndex<20)     regionPhiIndex = 2;
	if(phiIndex>=20 && phiIndex<28)     regionPhiIndex = 3;
	//set chire
	int chire=-99;
	if(chiSquared<1.)                   chire = 0;
	if(chiSquared>=1. && chiSquared<2.) chire = 1;
	if(chiSquared>=2. && chiSquared<3.) chire = 2;
	if(chiSquared>=3. && chiSquared<4.) chire = 3;
	//set clure
	int clure=-99;
	if(getHitClusize->at(j)==1) clure = 0;
	if(getHitClusize->at(j)==2) clure = 1;
	if(getHitClusize->at(j)==3) clure = 2;
	if(getHitClusize->at(j)==4) clure = 3;

	//apply cut
	if( getTrack>500 || fabs(incphi)>40 || chiSquared>4 || fabs(getD0)>10 || nscthits<7 || !f_pt1GeV ) continue;
	/////// pick up tracks with tight primary ///////
	if( !(f_nIBLBlayer) || getPixelholes!=0 ) continue; // !(f_nSi) ||
	if( nSihits<8 || nSiholes>1 || fabs(getZ0)>320 ) continue;

	// Fill histogram
	//side0
	if( getHitSide->at(j)==0 ){
	  //fill inter-strip
	  if(enum_phi!=5 && chire!=-99 && clure!=-99){
	    Strip_chi_side0[enum_phi+5*chire]->Fill(localx%80,1.);
	    if(chiSquared<=3){
	      if(enum_phi==3) Strip0->Fill(localx%80,1.);
	      Strip_clusize_side0[enum_phi+5*clure]->Fill(localx%80,1.);
	      Strip_step_side0[enum_phi+5*getHitLayer->at(j)+20*(abs(getHitEta->at(j))-1)]->Fill(localx%80,1.);
	      Strip_side0[enum_phi]->Fill(localx%80,1.);
	      Strip_region_side0[enum_phi+5*region]->Fill(localx%80,1.);
	      Strip_regionY_side0[enum_phi+5*regionY]->Fill(localx%80,1.);
	      Clusize_side0[enum_phi+5*getHitLayer->at(j)+20*(abs(getHitEta->at(j))-1)]->Fill(localx%80,getHitClusize->at(j));
	      Ntrk_side0[enum_phi+5*getHitLayer->at(j)+20*(abs(getHitEta->at(j))-1)]->Fill(localx%80,getTrack);
	      if(getHitLayer->at(j)==0)//     std::cout << "regionPhiIndex: " << regionPhiIndex << ", eta index: " << abs(getHitEta->at(j))-1 <<std::endl;
		Strip_Barrel3_side0[enum_phi+5*regionPhiIndex+20*(abs(getHitEta->at(j))-1)]->Fill(localx%80,1.);
	      if(enum_theta!=5){
		if(!f_pt5GeV)               Strip_fine_side0[0+3*enum_phi+15*enum_theta]->Fill(localx%80,1.);
		if(f_pt5GeV && !f_pt10GeV)  Strip_fine_side0[1+3*enum_phi+15*enum_theta]->Fill(localx%80,1.);
		if(f_pt10GeV)               Strip_fine_side0[2+3*enum_phi+15*enum_theta]->Fill(localx%80,1.);
	      }
	    }
	  }
	} // side0
	//side1
	if( getHitSide->at(j)==1 ){
	  //fill inter-strip
	  if(f_pt1GeV && enum_phi!=5 && chire!=-99 && clure!=-99){
	    Strip_chi_side1[enum_phi+5*chire]->Fill(localx%80,1.);
	    if(chiSquared<=3){
	      if(enum_phi==3) Strip1->Fill(localx%80,1.);
	      Strip_clusize_side1[enum_phi+5*clure]->Fill(localx%80,1.);
	      Strip_step_side1[enum_phi+5*getHitLayer->at(j)+20*(abs(getHitEta->at(j))-1)]->Fill(localx%80,1.);
	      Strip_side1[enum_phi]->Fill(localx%80,1.);
	      Strip_region_side1[enum_phi+5*region]->Fill(localx%80,1.);
	      Strip_regionY_side1[enum_phi+5*regionY]->Fill(localx%80,1.);
	      Clusize_side1[enum_phi+5*getHitLayer->at(j)+20*(abs(getHitEta->at(j))-1)]->Fill(localx%80,getHitClusize->at(j));
	      Ntrk_side1[enum_phi+5*getHitLayer->at(j)+20*(abs(getHitEta->at(j))-1)]->Fill(localx%80,getTrack);
	      if(getHitLayer->at(j)==0)     Strip_Barrel3_side1[enum_phi+5*regionPhiIndex+20*(abs(getHitEta->at(j))-1)]->Fill(localx%80,1.);
	      if(enum_theta!=5){
		if(!f_pt5GeV)               Strip_fine_side1[0+3*enum_phi+15*enum_theta]->Fill(localx%80,1.);
		if(f_pt5GeV && !f_pt10GeV)  Strip_fine_side1[1+3*enum_phi+15*enum_theta]->Fill(localx%80,1.);
		if(f_pt10GeV)               Strip_fine_side1[2+3*enum_phi+15*enum_theta]->Fill(localx%80,1.);
	      }
	    }
	  }
	} // side1
      }
    } // Hit loop
    

    ////////////////////////////
    ////////  Hole loop  ////////
    ////////////////////////////
    for( int j=0; j<nHoleElements; j++ ){

      // only Barrel
      if(getHoleBec->at(j)!=0) continue;

      // set variables
      bool f_pt1GeV = false;
      bool f_pt5GeV = false;
      bool f_pt10GeV = false;
      float incphi = 0.;
      float inctheta = 0.;
      int localx = 0;
      float localy = 0;
      int phiIndex = 0;
      int enum_phi = 0;
      int enum_theta = 0;

      //calculate variables
      if(getHoleLocaltheta->at(j)>M_PI/2){
	inctheta = ( getHoleLocaltheta->at(j) - M_PI )*180./M_PI;
      } else {
	inctheta = getHoleLocaltheta->at(j)*180./M_PI;
      }
      incphi = getHoleLocalphi->at(j)*180./M_PI;

      //Get healthy modules
      for(int h=0; h<bt; h++){
	if( getHoleBec->at(j)!=bec_index[h] || getHoleLayer->at(j)!=layer_index[h] || getHoleEta->at(j)!=eta_index[h] || getHolePhi->at(j)!=phi_index[h] ) continue;

	//get variables
	localx = (int)(getHoleLocalX->at(j)*1000+30680);
	if(localx>61360 || localx<0 ) continue;  // reduce 40um from sensor edge
	localy = getHoleLocalY->at(j);
	phiIndex = getHolePhi->at(j);

	if(getPt>1.){
	  f_pt1GeV = true;
	  if(getPt>5.){
	    f_pt5GeV = true;
	    if(getPt>10.){
	      f_pt10GeV = true;
	    }
	  }
	}
	enum_phi = SetPhi(incphi);
	enum_theta = SetTheta(inctheta);
	//set region
	int region=0;
	if(localx>=0 && localx<15340)       region = 0;
	if(localx>=15340 && localx<30680)   region = 1;
	if(localx>=30680 && localx<46020)   region = 2;
	if(localx>=46020 && localx<=61360)  region = 3;
	//set region for localY
	int regionY=-99;
	if(localy<-30)                      regionY = 0;
	if(localy>=-30 && localy<0)         regionY = 1;
	if(localy>=0   && localy<30)        regionY = 2;
	if(localy>=30)                      regionY = 3;
	//set region for phi_index
	int regionPhiIndex=-99;
	if(phiIndex>=28 || phiIndex<4)      regionPhiIndex = 0;
	if(phiIndex>=4  && phiIndex<12)     regionPhiIndex = 1;
	if(phiIndex>=12 && phiIndex<20)     regionPhiIndex = 2;
	if(phiIndex>=20 && phiIndex<28)     regionPhiIndex = 3;
	//set chire
	int chire=0;
	if(chiSquared<1.)                   chire = 0;
	if(chiSquared>=1. && chiSquared<2.) chire = 1;
	if(chiSquared>=2. && chiSquared<3.) chire = 2;
	if(chiSquared>=3. && chiSquared<4.) chire = 3;

	//apply cut
	//	if( getTrack>500 || fabs(incphi)>40 || chiSquared>4 || fabs(getD0)>10 || nscthits<7 || !f_pt1GeV || npixelhits==3 ) continue;
	if( getTrack>500 || fabs(incphi)>40 || chiSquared>4 || fabs(getD0)>10 || nscthits<6 || !f_pt1GeV ) continue;
	/////// pick up tracks with tight primary ///////
	if( !(f_nIBLBlayer) || getPixelholes!=0 ) continue; // !(f_nSi) ||
	if( nSihits<7 || nSiholes>2 || fabs(getZ0)>320 ) continue;

	// Fill histogram
	//side0
	if( getHoleSide->at(j)==0 ){
	  //fill inter-strip
	  if(f_pt1GeV && enum_phi!=5 && chire!=-99){
	    Strip_chi_side0[enum_phi+5*chire]->Fill(localx%80,0.);
	    if(chiSquared<=3){
	      if(enum_phi==3) Strip0->Fill(localx%80,0.);
	      for(int cr=0; cr<4; cr++){
		Strip_clusize_side0[enum_phi+5*cr]->Fill(localx%80,0.);
	      }
	      Strip_step_side0[enum_phi+5*getHoleLayer->at(j)+20*(abs(getHoleEta->at(j))-1)]->Fill(localx%80,0.);
	      Strip_side0[enum_phi]->Fill(localx%80,0.);
	      Strip_region_side0[enum_phi+5*region]->Fill(localx%80,0.);
	      Strip_regionY_side0[enum_phi+5*regionY]->Fill(localx%80,0.);
	      Ntrk_side0[enum_phi+5*getHitLayer->at(j)+20*(abs(getHitEta->at(j))-1)]->Fill(localx%80,getTrack);
	      if(getHoleLayer->at(j)==0)    Strip_Barrel3_side0[enum_phi+5*regionPhiIndex+20*(abs(getHoleEta->at(j))-1)]->Fill(localx%80,0.);
	      if(enum_theta!=5){
		if(!f_pt5GeV)               Strip_fine_side0[0+3*enum_phi+15*enum_theta]->Fill(localx%80,0.);
		if(f_pt5GeV && !f_pt10GeV)  Strip_fine_side0[1+3*enum_phi+15*enum_theta]->Fill(localx%80,0.);
		if(f_pt10GeV)               Strip_fine_side0[2+3*enum_phi+15*enum_theta]->Fill(localx%80,0.);
	      }
	    }
	  }
	} // side0
	//side1
	if( getHoleSide->at(j)==1 ){
	  //fill inter-strip
	  if(f_pt1GeV && enum_phi!=5 && chire!=-99){
	    Strip_chi_side1[enum_phi+5*chire]->Fill(localx%80,0.);
	    if(chiSquared<=3){
	      if(enum_phi==3) Strip1->Fill(localx%80,0.);
	      for(int cr=0; cr<4; cr++){
		Strip_clusize_side1[enum_phi+5*cr]->Fill(localx%80,0.);
	      }
	      Strip_step_side1[enum_phi+5*getHoleLayer->at(j)+20*(abs(getHoleEta->at(j))-1)]->Fill(localx%80,0.);
	      Strip_side1[enum_phi]->Fill(localx%80,0.);
	      Strip_region_side1[enum_phi+5*region]->Fill(localx%80,0.);
	      Strip_regionY_side1[enum_phi+5*regionY]->Fill(localx%80,0.);
	      Ntrk_side1[enum_phi+5*getHitLayer->at(j)+20*(abs(getHitEta->at(j))-1)]->Fill(localx%80,getTrack);
	      if(getHoleLayer->at(j)==0)    Strip_Barrel3_side1[enum_phi+5*regionPhiIndex+20*(abs(getHoleEta->at(j))-1)]->Fill(localx%80,0.);
	      if(enum_theta!=5){
		if(!f_pt5GeV)               Strip_fine_side1[0+3*enum_phi+15*enum_theta]->Fill(localx%80,0.);
		if(f_pt5GeV && !f_pt10GeV)  Strip_fine_side1[1+3*enum_phi+15*enum_theta]->Fill(localx%80,0.);
		if(f_pt10GeV)               Strip_fine_side1[2+3*enum_phi+15*enum_theta]->Fill(localx%80,0.);
	      }
	    }
	  }
	} // side1
      }
    } // Hole loop

    if( i % 100000 == 99999 ){
      std::cout << i+1 << " events have been done out of " << nEvents << " events." << std::endl;
    }
  } // Event loop

  //draw plots
  //TFile *fout = new TFile("/eos/atlas/unpledged/group-tokyo/users/rwakasa/SCT/EfficiencyLoopXY.root","recreate");
  //  TFile *fout = new TFile("EfficiencyLoopInterStrip.root","recreate");
  TFile *fout = new TFile(outputFile,"recreate");
  //TFile *fout = new TFile("/eos/atlas/unpledged/group-tokyo/users/rwakasa/SCT/EfficiencyLoopInterStrip.root","recreate");
  //TFile *fout = new TFile("/eos/atlas/unpledged/group-tokyo/users/rwakasa/SCT/EfficiencyLoopInterStrip310719.root","recreate");
  Strip0->Write();
  Strip1->Write();
  for(int i=0; i<120; i++){
    Strip_step_side0[i]->Write();
    Strip_step_side1[i]->Write();
    Clusize_side0[i]->Write();
    Clusize_side1[i]->Write();
    Strip_Barrel3_side0[i]->Write();
    Strip_Barrel3_side1[i]->Write();
    Ntrk_side0[i]->Write();
    Ntrk_side1[i]->Write();
  }
  /*
  for(int i=0; i<480; i++){
    Strip_regionY_side0[i]->Write();
    Strip_regionY_side1[i]->Write();
  }
  */
  for(int i=0; i<5; i++){
    Strip_side0[i]->Write();
    Strip_side1[i]->Write();
  }
  for(int i=0; i<75; i++){
    Strip_fine_side0[i]->Write();
    Strip_fine_side1[i]->Write();
  }
  for(int i=0; i<20; i++){
    Strip_region_side0[i]->Write();
    Strip_region_side1[i]->Write();
    Strip_clusize_side0[i]->Write();
    Strip_clusize_side1[i]->Write();
    Strip_chi_side0[i]->Write();
    Strip_chi_side1[i]->Write();
  }
  fout->Write();
  fout->Close();
  
  for(int i=0; i<5; i++){
    Strip_side0[i]->SetLineColor(kBlack);
    Strip_side1[i]->SetLineColor(kBlack);
    Strip_side0[i]->SetMarkerColor(5-i);
    Strip_side1[i]->SetMarkerColor(5-i);
    Strip_side0[i]->SetMarkerStyle(20);
    Strip_side1[i]->SetMarkerStyle(20);
  }
  for(int i=0; i<5; i++){
    for(int j=0; j<4; j++){
      for(int k=0; k<6; k++){
	Strip_step_side0[i+5*j+20*k]->SetLineColor(kBlack);
	Strip_step_side1[i+5*j+20*k]->SetLineColor(kBlack);
	Strip_step_side0[i+5*j+20*k]->SetMarkerColor(5-i);
	Strip_step_side1[i+5*j+20*k]->SetMarkerColor(5-i);
	Strip_step_side0[i+5*j+20*k]->SetMarkerStyle(20);
	Strip_step_side1[i+5*j+20*k]->SetMarkerStyle(20);
      }
    }
  }
  for(int i=0; i<5; i++){
    for(int j=0; j<4; j++){
	Strip_region_side0[i+5*j]->SetLineColor(kBlack);
	Strip_region_side1[i+5*j]->SetLineColor(kBlack);
	Strip_region_side0[i+5*j]->SetMarkerColor(5-i);
	Strip_region_side1[i+5*j]->SetMarkerColor(5-i);
	Strip_region_side0[i+5*j]->SetMarkerStyle(20);
	Strip_region_side1[i+5*j]->SetMarkerStyle(20);
    }
  }

  return 0;
}
